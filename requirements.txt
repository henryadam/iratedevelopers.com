Django==1.6.7
django-debug-toolbar==1.2.2
dj-database-url==0.3.0
dj-static==0.0.6
gunicorn==19.1.1
psycopg2==2.5.1
wsgiref==0.1.2
South==0.8.4




